﻿using Erika.Utils;
using HarmonyLib;
using System.IO;

namespace Erika;

public class OnStart
{
    [HarmonyPatch(typeof(Player), "OnCreateGame")]
    [HarmonyPostfix]
    public static void BeforeOnCreateGame(Player __instance)
    {
        CardBlueprint.SetRarity(Rarity.Legendary);
        Plugin.LogInfo("BeforeOnCreateGame");
    }


    [HarmonyPatch(typeof(Player), "CreateEquip")]
    [HarmonyPostfix]
    public static void AfterCreateEquip(Player __instance)
    {
        Chara chara = EClass.pc;
        //Plugin.LogInfo("Check job");

        // pickaxe for everyone
        var pickaxe = ThingGen.Create("pickaxe");
        pickaxe.ChangeMaterial("iron");
        chara.AddCard(pickaxe);


        switch (EClass.pc.job.id)
        {
            case "warrior":
                {
                    //Plugin.LogInfo("Warrior Add Sword");
                    SpawnThingWithMat("sword", 3, "grass");
                }
                break;
            case "thief":
                {
                    //Plugin.LogInfo("Thief Add 2 dagger");
                    SpawnThingWithMat("dagger", 2, "grass");
                    SpawnThingWithMat("dagger", 1, "paper");
                }
                break;
            case "archer":
                {
                    //Plugin.LogInfo("Archer Add bow");
                    chara.AddCard(ThingGen.Create("arrow").SetNum(120));
                    SpawnThingWithMat("bow", 2, "mica");
                }
                break;
            case "warmage":
                {
                    //Plugin.LogInfo("Warmage Add Sword");
                    SpawnThingWithMat("sword", 2, "paper");
                }
                break;
            case "farmer":
                {
                    //Plugin.LogInfo("Farmer Add tools");
                    SpawnThing("waterPot", 1);
                    SpawnThing("seed", 1);
                    SpawnThing("seed", 1);
                    SpawnThing("seed", 1);
                    chara.AddCard(TraitSeed.MakeSeed("wheat")).SetNum(20);

                    SpawnThingWithMat("shovel", -1, "iron");
                    SpawnThingWithMat("hoe", -1, "iron");
                    SpawnThingWithMat("sickle", -1, "iron");


                }
                break;
            case "gunner":
                {
                    //Plugin.LogInfo("Gunner Add gun");
                    SpawnThing("bullet", 200);
                    SpawnThingWithMat("gun", 2, "iron");
                }
                break;

            case "tourist":
                {
                    //Plugin.LogInfo("Tourist Add ... some thing");
                    SpawnThing("water", 1);
                    SpawnThing("potion", 1);
                    SpawnThing("bait", ErikaRand.Next(4, 6));
                    SpawnThing("fishingRod", 1);

                }
                break;

            case "pianist":
                {
                    //Plugin.LogInfo("Pianist Add ... lute");
                    SpawnThingWithMat("lute", 1, "silver");
                }
                break;

            case "priest":
                {
                    //Plugin.LogInfo("Pianist Add rod");
                    SpawnThingWithMat("rod", 2, "mica");
                }
                break;
        }

        foreach (var thing in chara.things)
        {
            thing.Identify(false, IDTSource.SuperiorIdentify);
        }

        GenList.WriteToFile("./");

        //void SpawnRandomThing(string[] things, int num)
        //{
        //    chara.AddCard(ThingGen.Create(things[Rand.Next(0, things.Length)]).SetNum(num));
        //}

        void SpawnThing(string thing, int num)
        {
            CardBlueprint.SetNormalRarity();
            chara.AddCard(ThingGen.Create(thing).SetNum(num));
        }

        void SpawnThingWithMat(string thing, int lv, string mat)
        {
            CardBlueprint.SetNormalRarity();
            var t = ThingGen.Create(thing, -1, lv);
            t.ChangeMaterial(mat);
            chara.AddCard(t);
        }

    }
}
