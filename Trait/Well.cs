﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Erika;

public class Well
{
    [HarmonyPatch(typeof(TraitWell), "TrySetAct")]
    [HarmonyPrefix]
    public static bool OnDrinkWater(TraitWell __instance, ref ActPlan p)
    {
        p.TrySetAct("actDrink", delegate
        {
            if (__instance.Charges <= 0)
            {
                EClass.pc.Say("drinkWell_empty", EClass.pc, __instance.owner);
                return false;
            }
            var rnd = EClass.rnd(1001);


            EClass.pc.Say("drinkWell", EClass.pc, __instance.owner);
            EClass.pc.PlaySound("drink");
            EClass.pc.PlayAnime(AnimeID.Shiver);

            if (__instance.polluted)
            {
                EClass.pc.Say("drinkWater_dirty");
                TraitWell.BadEffect(EClass.pc);
                ActEffect.Proc(EffectId.ModPotential, EClass.pc, null, -100);

            }
            else if (__instance.IsHoly || rnd <= 700)
            {
                ActEffect.Proc(EffectId.ModPotential, EClass.pc, null, 100);
            }
            else if (rnd <= 900)
            {
                //ActEffect.Proc(EffectId.Mutation, EClass.pc);
                if(!EClass.pc.MutateRandom(1, 100, ether: false, BlessedState.Blessed))
                {
                    ActEffect.Proc(EffectId.ModPotential, EClass.pc, null,100);
                }
            }
            else if (rnd <= 985)
            {
                if (EClass.rnd(2) == 0)
                    ActEffect.Proc(EffectId.RestoreBody, EClass.pc, EClass.pc);
                else
                    ActEffect.Proc(EffectId.RestoreMind, EClass.pc, EClass.pc);
            }
            else if (rnd <= 990)
            {
                ActEffect.Proc(EffectId.Ally, EClass.pc, EClass.pc);

            }
            else if (rnd <= 995)
            {
                if (EClass.rnd(2) == 0)
                    ActEffect.Proc(EffectId.EnhanceBody, EClass.pc, EClass.pc);
                else
                    ActEffect.Proc(EffectId.EnhanceMind, EClass.pc, EClass.pc);
            }
            else
            {
                ActEffect.Proc(EffectId.Wish, EClass.pc);
                EClass.player.wellWish = true;
            }

            if (!__instance.polluted)
                EClass.pc.Say("drinkWater_clear");

            __instance.ModCharges(-1);
            return true;
        }, __instance.owner);

        return false;
    }
}
