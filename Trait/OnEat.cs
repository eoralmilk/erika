﻿using HarmonyLib;

namespace Erika;

public class OnEat
{

    [HarmonyPatch(typeof(Trait), "OnEat")]
    [HarmonyPostfix]
    public static void OnBecameEaten(Trait __instance, ref Chara c)
    {
        var eater = CAM.GetAddon(c);
        eater.OnConsumedFood(__instance.owner);
    }
}
