﻿using BepInEx;
using HarmonyLib;


namespace Erika;

[BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
public class Plugin : BaseUnityPlugin
{
    public static Plugin GetPlugin;



    private void Awake()
    {
        GetPlugin = this;
        Harmony.CreateAndPatchAll(typeof(CardMisc));


        Harmony.CreateAndPatchAll(typeof(MakePlayerChara));
        Harmony.CreateAndPatchAll(typeof(CharaMisc));
        Harmony.CreateAndPatchAll(typeof(OnAttack));
        Harmony.CreateAndPatchAll(typeof(OnClickInvOwner));
        Harmony.CreateAndPatchAll(typeof(OnSteal));
        Harmony.CreateAndPatchAll(typeof(OnTurn));
        Harmony.CreateAndPatchAll(typeof(ErikaStats));

        Harmony.CreateAndPatchAll(typeof(OnStart));

        Harmony.CreateAndPatchAll(typeof(ThingGenerate));
        Harmony.CreateAndPatchAll(typeof(ThingMisc));

        Harmony.CreateAndPatchAll(typeof(OnShop));
        Harmony.CreateAndPatchAll(typeof(Well));

        //Harmony.CreateAndPatchAll(typeof(OnEat));

        // Plugin startup logic
        Logger.LogInfo($"Plugin {PluginInfo.PLUGIN_GUID} is loaded! Erika Meow!!");
    }

    public static void LogInfo(in string info)
    {
        GetPlugin.Logger.LogInfo(info);
    }
}
