﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Erika;

public class ThingGenerate
{
    [HarmonyPatch(typeof(Card), "Duplicate")]
    [HarmonyPrefix]
    public static bool OnDuplicate(Card __instance, ref Thing __result)
    {
        if (__instance.isChara)
        {
            Thing thing = ThingGen.Create("figure");
            thing.MakeFigureFrom(__instance.id);
            __result = thing;
            return false;
        }

        return true;
    }
}
