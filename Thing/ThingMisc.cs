﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Text;
using static TextureReplace;

namespace Erika;

public class ThingMisc
{
    [HarmonyPatch(typeof(Thing), "GetHoverText")]
    [HarmonyPostfix]
    public static void GetThingHoverText(Thing __instance, ref string __result)
    {
        string text = "";

        if (__instance.isNPCProperty)
            text += "(x)";
        __result += "Lv." + __instance.LV + " " + text;
    }


    //[HarmonyPatch(typeof(Thing), "WriteNote")]
    //[HarmonyPostfix]
    //public static void WriteNote(Thing __instance, ref UINote n, ref Action<UINote> onWriteNote)
    //{
    //    string text = "";

    //    if (__instance.isNPCProperty)
    //        text += "(x)";
    //    __result += "Lv." + __instance.LV + " " + text;
    //}
}
