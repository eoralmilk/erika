﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Text;
using static QuestCraft;
using static UnityEngine.GridBrushBase;

namespace Erika;

public class CardMisc
{

    [HarmonyPatch(typeof(Card), "GetHoverText")]
    [HarmonyPostfix]
    public static void GetCardHoverText(Card __instance, ref string __result)
    {
        if (__instance.isChara || __instance.isThing)
            return;

        string text = "";

        if (__instance.isNPCProperty)
            text += "(x)";
        __result += "Lv." + __instance.LV + " " + text;
    }

    [HarmonyPatch(typeof(BaseTaskHarvest), "GetTextDifficulty")]
    [HarmonyPostfix]
    public static void GetTextDifficulty(BaseTaskHarvest __instance, ref string __result)
    {
        if (__instance.pos.cell.growth != null)
            __result += " growth." + __instance.pos.cell.growth.stage.idx;
        __result += " Lv." + __instance.reqLv + (__instance.IsTooHard ? " > me." : " <= me.") + __instance.toolLv ;
    }
    
}
