﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Erika;

public class OnClickInvOwner
{
    [HarmonyPatch(typeof(InvOwner), "AllowHold")]
    [HarmonyPrefix]
    public static bool ReplaceAllowHold(InvOwner __instance, ref Thing t, ref bool __result)
    {
        if (!t.trait.CanBeDropped)
        {
            __result = false;
            return false;
        }

        int fav = 0;
        if (__instance.owner.isChara)
        {
            fav = (__instance.owner as Chara)._affinity;
        }

        if (__instance.Container.isChara && !__instance.Container.IsPC)
        {
            //Plugin.LogInfo(__instance.owner.NameSimple + " affinity: " + fav);
            if (t.HasTag(CTAG.gift))
            {
                __result = false;
                return false;
            }

            if (t.isGifted && fav < 150)
            {
                __result = false;
                return false;
            }


            if ((t.isEquipped || t.isNPCProperty || t.id == "money") && fav < 75)
            {
                __result = false;
                return false;
            }

            t.isNPCProperty = false;
            t.isGifted = false;
            __result =  true;
            return false;
        }

        __result = !t.trait.CanOnlyCarry && (!__instance.Container.isNPCProperty || !InvOwner.FreeTransfer);

        return false;
    }
}
