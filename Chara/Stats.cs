﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Text;

namespace Erika;

public class ErikaStats
{

    [HarmonyPatch(typeof(StatsStamina), "Mod")]
    [HarmonyPrefix]
    public static bool OnMod(StatsStamina __instance, ref int a)
    {
        var ca = CAM.GetAddon(__instance.Owner);
        ca.ResetStaminaTick();
        if (ca.LockStamina)
            return false;

        return true;
    }
}