﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Text;

namespace Erika;

public class OnTurn
{
    [HarmonyPatch(typeof(Chara), "Tick")]
    [HarmonyPostfix]
    public static void AfterTick(Chara __instance)
    {
        var ca = CAM.GetAddon(__instance);
        ca.Tick();
    }

    [HarmonyPatch(typeof(Chara), "WillConsumeTurn")]
    [HarmonyPrefix]
    public static bool OnWillConsumeTurn(Chara __instance, ref bool __result)
    {
        if (CAM.GetAddon(__instance).WillConsumeTurn)
        {
            __result = true;
            return false;
        }
        else
            return true;
    }
}
