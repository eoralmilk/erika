﻿using Dungen;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Unity.Jobs;
using UnityEngine;

namespace Erika;

public class CharaMisc
{
    [HarmonyPatch(typeof(Chara), "GetHoverText")]
    [HarmonyPostfix]
    public static void GetHoverText(Chara __instance, ref string __result)
    {
        string text = __instance.Name;
        if (__instance.IsFriendOrAbove())
        {
            text = text.TagColor(EClass.Colors.colorFriend);
        }
        else if (__instance.IsHostile())
        {
            text = text.TagColor(EClass.Colors.colorHostile);
        }

        int num = 2;
        int lV = EClass.pc.LV;
        if (__instance.LV >= lV * 5)
        {
            num = 0;
        }
        else if (__instance.LV >= lV * 2)
        {
            num = 1;
        }
        else if (__instance.LV <= lV / 4)
        {
            num = 4;
        }
        else if (__instance.LV <= lV / 2)
        {
            num = 3;
        }

        string text2 = "Lv." + __instance.LV + "  " + Lang.GetList("lvComparison")[num];
        text2 = (" (" + text2 + ") ").TagSize(14).TagColor(EClass.Colors.gradientLVComparison.Evaluate(0.25f * (float)num));
        string s = (__instance.IsFriendOrAbove() ? "HostilityAlly" : (__instance.IsNeutral() ? "HostilityNeutral" : "HostilityEnemy"));
        s = (" (" + s.lang() + ") ").TagSize(14);
        if (!EClass.pc.IsMoving)
        {
            if (EClass.pc.HasHigherGround(__instance))
            {
                text2 += "lowerGround".lang();
            }
            else if (__instance.HasHigherGround(EClass.pc))
            {
                text2 += "higherGround".lang();
            }
        }

        string text3 = "";
        if (__instance.knowFav)
        {
            text3 += Environment.NewLine;
            text3 = text3 + "<size=14>" + "favgift".lang(__instance.GetFavCat().GetName().ToLower(), __instance.GetFavFood().GetName()) + "</size>";
        }

        string text4 = "";
        if (EClass.debug.showExtra)
        {
            text4 += Environment.NewLine;
            text4 = text4 + "Lv:" + __instance.LV + "  HP:" + __instance.hp + "/" + __instance.MaxHP + "  MP:" + __instance.mana.value + "/" + __instance.mana.max + "  DV:" + __instance.DV + "  PV:" + __instance.PV + "  Hunger:" + __instance.hunger.value;
            text4 += Environment.NewLine;
            text4 = text4 + "Global:" + __instance.IsGlobal + "  AI:" + __instance.ai;
        }

        __result = text + text2 + text3 + text4;
    }
}
