﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Erika;

public class OnAttack
{

    public static int numAmmo = 0;


    [HarmonyPatch(typeof(ActMelee), "Perform")]
    [HarmonyPrefix]
    public static bool BeforeMelee(ActMelee __instance)
    {
        CAM.GetAddon(Act.CC).BeforeAct(CharaAddon.ActType.MeleeAtk);
        return true;
    }


    [HarmonyPatch(typeof(ActMelee), "Perform")]
    [HarmonyPostfix]
    public static void AfterMelee(ActMelee __instance)
    {
        CAM.GetAddon(Act.CC).AfterAct(CharaAddon.ActType.MeleeAtk, Act.CC, Act.TC);
        CAM.GetAddon(Act.CC).ResetStaminaTick();
    }

    [HarmonyPatch(typeof(ActRanged), "Perform")]
    [HarmonyPrefix]
    public static bool BeforeRanged(ActRanged __instance)
    {
        if (Act.CC.ranged != null)
            numAmmo = Act.CC.ranged.c_ammo;
        else
        {
            numAmmo = -1;
            //Plugin.LogInfo("Null ranged");
        }

        CAM.GetAddon(Act.CC).BeforeAct(CharaAddon.ActType.RangedAtk);
        return true;
    }

    [HarmonyPatch(typeof(ActRanged), "Perform")]
    [HarmonyPostfix]
    public static void AfterRanged(ActRanged __instance)
    {
        if (Act.CC.ranged != null && numAmmo >= 0 && Act.CC.ranged.c_ammo != numAmmo)
            CAM.GetAddon(Act.CC).AfterAct(CharaAddon.ActType.RangedAtk, Act.CC, Act.TC);
        else
            CAM.GetAddon(Act.CC).AfterAct(CharaAddon.ActType.RangedAtkFailed, Act.CC, Act.TC);


        CAM.GetAddon(Act.CC).ResetStaminaTick();

    }
}
