﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Text;
using static SourceData;

namespace Erika;

public class MakePlayerChara
{
    [HarmonyPatch(typeof(UICharaMaker), "BuildRaces")]
    [HarmonyPrefix]
    public static bool AfterBuildRaces(UICharaMaker __instance)
    {
        __instance.races.Clear();
        __instance.jobs.Clear();
        bool flag = EMono.core.config.test.extraRace;
        foreach (SourceRace.Row row in ((SourceData<SourceRace.Row, string>)(object)EMono.sources.races).rows)
        {
            if (__instance.extraRace)
            {
                __instance.races.Add(row);
            }
            else if (row.playable == 1 || (flag && row.playable != 9))
            {
                __instance.races.Add(row);
            }
        }

        foreach (SourceJob.Row row2 in ((SourceData<SourceJob.Row, string>)(object)EMono.sources.jobs).rows)
        {
            if (row2.playable == 1 || (row2.playable <= 6 && __instance.extraRace) || (flag && row2.playable != 9))
            {
                __instance.jobs.Add(row2);
            }
        }

        __instance.races.Sort((SourceRace.Row a, SourceRace.Row b) => (a.playable - b.playable) * 10000 + ((BaseRow)a)._index - ((BaseRow)b)._index);
        __instance.jobs.Sort((SourceJob.Row a, SourceJob.Row b) => (a.playable - b.playable) * 10000 + ((BaseRow)a)._index - ((BaseRow)b)._index);

        return false;
    }
}
