﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erika.Utils;

public static class ErikaRand
{
    public static int Next(int startInclusive, int endExclusive)
    {
        return UnityEngine.Random.Range(startInclusive, endExclusive);
    }
}
