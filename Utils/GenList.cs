﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Erika.Utils;

public class GenList
{

    public static void WriteToFile(string path)
    {
        if (!File.Exists(Path.Combine(path, "ListThing.txt")))
        {
            File.WriteAllText(Path.Combine(path, "ListThing.txt"), ListThing());
        }

        if (!File.Exists(Path.Combine(path, "ListChara.txt")))
        {
            File.WriteAllText(Path.Combine(path, "ListChara.txt"), ListChara());
        }

        if (!File.Exists(Path.Combine(path, "ListElements.txt")))
        {
            File.WriteAllText(Path.Combine(path, "ListElements.txt"), ListElements());
        }

        if (!File.Exists(Path.Combine(path, "ListMaterials.txt")))
        {
            File.WriteAllText(Path.Combine(path, "ListMaterials.txt"), ListMaterials());
        }

        if (!File.Exists(Path.Combine(path, "ListCategories.txt")))
        {
            File.WriteAllText(Path.Combine(path, "ListCategories.txt"), ListCategories());
        }
    }

    public static string ListChara()
    {
        string text = "";
        foreach (SourceChara.Row row in EClass.sources.charas.rows)
        {
            text = text + row.id +"\t\t " + row.name + "\n";
        }

        return text;
    }


    public static string ListThing()
    {
        string text = "";
        foreach (SourceThing.Row row in EClass.sources.things.rows)
        {
            text = text + row.id + "\t\t " + row.name + "\n";
        }

        return text;
    }

    public static string ListArea()
    {
        string text = "";
        foreach (SourceArea.Row row in EClass.sources.areas.rows)
        {
            text = text + row.id + "\t\t " + row.name + "\n";
        }

        return text;
    }

    public static string ListElements()
    {
        string text = "";
        foreach (var row in EClass.sources.elements.rows)
        {
            text = text + row.id + "\t\t " + row.name + "\n";
        }

        return text;
    }

    public static string ListMaterials()
    {
        string text = "";
        foreach (var row in EClass.sources.materials.rows)
        {
            text = text + row.id + "\t\t " + row.name + "\n";
        }

        return text;
    }

    public static string ListCategories()
    {
        string text = "";
        foreach (var row in EClass.sources.categories.rows)
        {
            text = text + row.id + "\t\t " + row.name + "\n";
        }

        return text;
    }

    public static string ListTactics()
    {
        string text = "";
        foreach (var row in EClass.sources.tactics.rows)
        {
            text = text + row.id + "\t\t " + row.name + "\n";
        }

        return text;
    }
}
